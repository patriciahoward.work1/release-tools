# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::SyncRemotesService do
  let(:auto_deploy_branch) { '13-0-auto-deploy-20200520' }

  before do
    allow(ReleaseTools::AutoDeployBranch)
      .to receive(:current_name)
      .and_return(auto_deploy_branch)
  end

  describe '#execute' do
    it 'syncs master and auto-deploy branches for every project' do
      service = described_class.new

      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabEe, 'master', auto_deploy_branch)
      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::GitlabCe, 'master')
      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::OmnibusGitlab, 'master', auto_deploy_branch)
      expect(service).to receive(:sync_branches)
        .with(ReleaseTools::Project::Gitaly, 'master')

      service.execute
    end

    # https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/943
    context 'when `auto_deploy_on_security` is enabled' do
      before do
        enable_feature(:auto_deploy_on_security)
      end

      it 'excludes the auto-deploy branch' do
        service = described_class.new

        expect(service).to receive(:sync_branches)
          .with(ReleaseTools::Project::GitlabEe, 'master')
        expect(service).to receive(:sync_branches)
          .with(ReleaseTools::Project::GitlabCe, 'master')
        expect(service).to receive(:sync_branches)
          .with(ReleaseTools::Project::OmnibusGitlab, 'master')
        expect(service).to receive(:sync_branches)
          .with(ReleaseTools::Project::Gitaly, 'master')

        service.execute
      end
    end
  end
end
