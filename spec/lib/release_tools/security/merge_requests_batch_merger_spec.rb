# frozen_string_literal: true

require 'spec_helper'

# rubocop: disable RSpec/NestedGroups
describe ReleaseTools::Security::MergeRequestsBatchMerger do
  let(:client) { double(:client) }
  let(:issues_fetcher) { double(:issues_fetcher) }
  let(:cherry_picker) { double(:cherry_picker) }

  let(:batch_merger_result) do
    double(:result, invalid: [], pending: Hash.new([]))
  end

  let(:batch_merger) { described_class.new(client) }

  before do
    allow(ReleaseTools::Security::IssuesFetcher)
      .to receive(:new)
      .and_return(issues_fetcher)

    allow(ReleaseTools::Security::BatchMergerResult)
      .to receive(:new)
      .and_return(batch_merger_result)

    allow(ReleaseTools::Security::CherryPicker)
      .to receive(:new)
      .and_return(cherry_picker)

    allow(ReleaseTools::Slack::ChatopsNotification)
      .to receive(:security_merge_requests_processed)
      .and_return(nil)
  end

  describe '#execute' do
    context 'when security implementation issues are not ready' do
      it 'does not process the issues' do
        allow(issues_fetcher)
          .to receive(:execute)
          .and_return([])

        expect(batch_merger)
          .not_to receive(:validated_merge_requests)

        expect(batch_merger)
          .not_to receive(:merge_in_batches)

        batch_merger.execute
      end
    end

    context 'with security issues ready to be processed' do
      let(:response) { double(:response) }
      let(:author) { double(:author, id: 1, username: 'joe') }

      let(:mr1) do
        double(
          :merge_request,
          iid: 1,
          project_id: 1,
          target_branch: 'master',
          web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1',
          author: author
        )
      end

      let(:mr2) do
        double(
          :merge_request,
          iid: 2,
          project_id: 1,
          target_branch: '12-10-stable-ee',
          web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2',
          author: author
        )
      end

      let(:mr3) do
        double(
          :merge_request,
          iid: 3,
          project_id: 1,
          target_branch: '12-9-stable-ee',
          web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/3',
          author: author
        )
      end

      let(:mr4) do
        double(
          :merge_request,
          iid: 4,
          project_id: 1,
          target_branch: '12-8-stable-ee',
          web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/4',
          author: author
        )
      end

      let(:merge_requests) do
        [mr1, mr2, mr3, mr4]
      end

      let(:issue1) do
        double(
          :issue,
          iid: 1,
          web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/issues/1',
          merge_requests: merge_requests,
          merge_request_targeting_master: mr1,
          merge_requests_targeting_stable: [mr2, mr3, mr4],
          project_with_auto_deploy?: true
        )
      end

      let(:issue2) do
        double(
          :issue,
          iid: 2,
          web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/issues/2',
          merge_requests: merge_requests,
          merge_request_targeting_master: mr1,
          merge_requests_targeting_stable: [mr2, mr3, mr4],
          project_with_auto_deploy?: true
        )
      end

      context 'with issues with invalid merge requests' do
        before do
          allow(issues_fetcher)
            .to receive(:execute)
            .and_return([issue1, issue2])

          allow(batch_merger)
            .to receive(:validated_merge_requests)
            .with(merge_requests)
            .and_return([[mr1], [mr2, mr3, mr4]])

          allow(client)
            .to receive(:update_merge_request)
            .and_return(response)

          allow(client)
            .to receive(:create_merge_request_discussion)
            .and_return(response)
        end

        it 'creates a discussion on the merge request targeting master' do
          discussion = <<~ERROR.strip
            @joe

            Some of the merge requests associated with #1 are not ready
            to be merged. Please review them, fix any problems reported and resolve all
            merge conflicts (in case there are any).

            Once resolved and the pipelines have passed, assign all merge
            requests back to me and mark this discussion as resolved.

            #{ReleaseTools::Security::MergeRequestsValidator::ERROR_FOOTNOTE}
          ERROR

          expect(client)
            .to receive(:create_merge_request_discussion)
            .with(mr1.project_id, mr1.iid, body: discussion)

          without_dry_run do
            batch_merger.execute
          end
        end

        it 'reassigns all the merge requests back to the author' do
          expect(client)
            .to receive(:update_merge_request)
            .exactly(8).times

          without_dry_run do
            batch_merger.execute
          end
        end

        it 'does not pick the merge request targeting master into auto deploy branch' do
          expect(batch_merger)
            .not_to receive(:cherry_pick_into_auto_deploy)

          batch_merger.execute
        end

        it 'notifies the results' do
          expect(ReleaseTools::Slack::ChatopsNotification)
            .to receive(:security_merge_requests_processed)

          batch_merger.execute
        end
      end

      context 'with issues with valid merge requests' do
        context 'when all the merge requests can be merged' do
          let(:mr1) do
            double(
              :merge_request,
              iid: 1,
              project_id: 1,
              target_branch: 'master',
              web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1',
              author: author,
              merge_commit_sha: '1a2b3c'
            )
          end

          before do
            allow(issues_fetcher)
              .to receive(:execute)
              .and_return([issue1, issue2])

            allow(batch_merger)
              .to receive(:validated_merge_requests)
              .with(merge_requests)
              .and_return([[mr1, mr2, mr3, mr4], []])

            merged_mr = double(:merge_request,
                               merge_commit_sha: '1a2b3c',
                               target_branch: 'master',
                              web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1')

            allow(client)
              .to receive(:accept_merge_request)
              .with(mr1.project_id, mr1.iid, squash: true)
              .and_return(merged_mr)

            [mr2, mr3, mr4].each do |mr|
              allow(client)
                .to receive(:accept_merge_request)
                .with(mr.project_id, mr.iid, squash: true)
                .and_return(nil)
            end

            allow(cherry_picker)
              .to receive(:execute)
              .and_return(nil)
          end

          it 'merges the merge requests in batch' do
            expect(client)
              .to receive(:accept_merge_request)
              .exactly(8).times

            without_dry_run do
              batch_merger.execute
            end
          end

          it 'picks the merge requests targeting master into auto-deploy branch' do
            expect(cherry_picker)
              .to receive(:execute)
              .twice

            without_dry_run do
              batch_merger.execute
            end
          end

          it 'notifies the result' do
            expect(ReleaseTools::Slack::ChatopsNotification)
              .to receive(:security_merge_requests_processed)

            batch_merger.execute
          end
        end

        context "with merge requests that couldn't be merged" do
          before do
            allow(issues_fetcher)
              .to receive(:execute)
              .and_return([issue1])

            allow(batch_merger)
              .to receive(:validated_merge_requests)
              .with(merge_requests)
              .and_return([[mr1, mr2, mr3, mr4], []])

            allow(batch_merger_result.pending)
              .to receive(:key?)
              .and_return(true)

            mr_without_commit_sha = double(:merge_request, merge_commit_sha: nil)

            allow(client)
              .to receive(:accept_merge_request)
              .and_return(mr_without_commit_sha)
          end

          it 'marks the merge requests as pending' do
            expect(batch_merger_result)
              .to receive(:pending)
              .exactly(4).times

            without_dry_run do
              batch_merger.execute
            end
          end

          it 'does not pick the merge request targeting master to auto-deploy branch' do
            expect(batch_merger)
              .not_to receive(:cherry_pick_into_auto_deploy)

            without_dry_run do
              batch_merger.execute
            end
          end

          it 'notifies the result' do
            expect(ReleaseTools::Slack::ChatopsNotification)
              .to receive(:security_merge_requests_processed)

            batch_merger.execute
          end
        end

        context 'when security_release_auto_deploy_experiment is enabled' do
          let(:batch_merger) do
            described_class.new(client, merge_master: merge_master)
          end

          before do
            enable_feature(:security_release_auto_deploy_experiment)

            allow(issues_fetcher)
              .to receive(:execute)
              .and_return([issue1])

            allow(batch_merger)
              .to receive(:validated_merge_requests)
              .with(merge_requests)
              .and_return([[mr1, mr2, mr3, mr4], []])

            merged_mr = double(:merge_request,
                               merge_commit_sha: '1a2b3c',
                               target_branch: 'master',
                               web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1')

            allow(client)
              .to receive(:accept_merge_request)
              .and_return(merged_mr)

            allow(cherry_picker)
              .to receive(:execute)
              .and_return(nil)
          end

          context 'when merge_master is true' do
            let(:merge_master) { true }

            it 'merges merge request targeting master and cherry picks it' do
              mr_master = issue1.merge_request_targeting_master

              expect(client)
                .to receive(:accept_merge_request)
                .with(mr_master.project_id, mr_master.iid, squash: true)
                .once

              expect(cherry_picker)
                .to receive(:execute)
                .once

              without_dry_run do
                batch_merger.execute
              end
            end

            it 'ignores merge requests targeting stable branches' do
              mrs_stable = issue1.merge_requests_targeting_stable

              mrs_stable.each do |mr|
                expect(client)
                  .not_to receive(:accept_merge_request)
                  .with(mr.project_id, mr.iid, squash: true)
              end

              without_dry_run do
                batch_merger.execute
              end
            end
          end

          context 'when merge_master is false' do
            let(:merge_master) { false }

            it 'merges backports' do
              backports = issue1.merge_requests_targeting_stable

              backports.each do |mr|
                expect(client)
                  .to receive(:accept_merge_request)
                  .with(mr.project_id, mr.iid, squash: true)
                  .once
              end

              expect(cherry_picker).not_to receive(:execute)

              without_dry_run do
                batch_merger.execute
              end
            end

            it 'ignores merge request targeting master' do
              mr_master = issue1.merge_request_targeting_master

              expect(client)
                .not_to receive(:accept_merge_request)
                .with(mr_master.project_id, mr_master.iid, squash: true)

              without_dry_run do
                batch_merger.execute
              end
            end
          end
        end
      end
    end
  end
end

# rubocop: enable RSpec/NestedGroups
