# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::IssuesResult do
  let(:issue1) do
    double(
      :issue,
      iid: 1,
      web_url: 'https://gitlab.com/gitlab-org/security/gitlab/issues/1'
    )
  end

  let(:issue2) do
    double(
      :issue,
      iid: 2,
      web_url: 'https://gitlab.com/gitlab-org/security/gitlab/issues/2'
    )
  end

  let(:issue3) do
    double(
      :issue,
      iid: 3,
      web_url: 'https://gitlab.com/gitlab-org/security/gitlab/issues/3'
    )
  end

  let(:issue4) do
    double(
      :issue,
      iid: 4,
      web_url: 'https://gitlab.com/gitlab-org/security/gitlab/issues/4'
    )
  end

  let(:issues) { [issue1, issue2, issue3, issue4] }

  describe '#slack_attachments' do
    let(:issues_result) do
      described_class.new(
        issues: issues,
        not_ready: [issue1, issue2],
        ready: [issue3, issue4]
      )
    end

    it 'includes information about the issues' do
      total_output = issues_result.slack_attachments[0]

      expect(total_output).to match(
        a_hash_including(
          fallback: 'Security Implementation Issues associated: 4.',
          title: ':information_source: Security Implementation Issues associated: 4.',
          color: 'good'
        )
      )

      issues_not_ready_output = issues_result.slack_attachments[1]

      issues_not_ready_information = [
        {
          title: "Security implementation issue: ##{issue1.iid}",
          value: "<#{issue1.web_url}>",
          short: false
        },
        {
          title: "Security implementation issue: ##{issue2.iid}",
          value: "<#{issue2.web_url}>",
          short: false
        }
      ]

      expect(issues_not_ready_output).to match(
        a_hash_including(
          fallback: 'Not ready to be processed: 2.',
          title: ':status_warning: Issues not ready to be processed: 2.',
          fields: issues_not_ready_information
        )
      )

      issues_ready_output = issues_result.slack_attachments[2]

      issues_ready_information = [
        {
          title: "Security implementation issue: ##{issue3.iid}",
          value: "<#{issue3.web_url}>",
          short: false
        },
        {
          title: "Security implementation issue: ##{issue4.iid}",
          value: "<#{issue4.web_url}>",
          short: false
        }
      ]

      expect(issues_ready_output).to match(
        a_hash_including(
          fallback: 'Ready to be processed: 2.',
          title: ':ballot_box_with_check: Issues ready to be processed: 2.',
          fields: issues_ready_information
        )
      )
    end

    context 'without issues ready to be processed' do
      let(:issues_result) do
        described_class.new(
          issues: issues,
          not_ready: issues,
          ready: []
        )
      end

      it 'return no information for issues ready to be processed' do
        result = issues_result.slack_attachments

        expect(result[0]).to be_present
        expect(result[1]).to be_present
        expect(result[2]).to be_empty
      end
    end

    context 'without issues not ready to be processed' do
      let(:issues_result) do
        described_class.new(
          issues: issues,
          not_ready: [],
          ready: issues
        )
      end

      it 'return no information for issues ready to be processed' do
        result = issues_result.slack_attachments

        expect(result[0]).to be_present
        expect(result[1]).to be_empty
        expect(result[2]).to be_present
      end
    end
  end
end
