# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::BatchMergerResult do
  let(:batch_merger_result) { described_class.new }

  let(:issue1) do
    double(
      :issue,
      iid: 1,
      web_url: 'https://gitlab.com/gitlab-org/security/gitlab/issues/1'
    )
  end

  let(:issue2) do
    double(
      :issue,
      iid: 2,
      web_url: 'https://gitlab.com/gitlab-org/security/gitlab/issues/2'
    )
  end

  describe '#slack_attachments' do
    context 'when security issues have invalid MRs' do
      it 'includes information about the issues' do
        batch_merger_result.invalid << issue1
        batch_merger_result.invalid << issue2

        invalid_issues_output = batch_merger_result.slack_attachments[0]

        invalid_issues_fields = [
          {
            title: "Security implementation issue: ##{issue1.iid}",
            value: "<#{issue1.web_url}>",
            short: false
          },
          {
            title: "Security implementation issue: ##{issue2.iid}",
            value: "<#{issue2.web_url}>",
            short: false
          }
        ]

        expect(invalid_issues_output).to match(
          a_hash_including(
            fallback: 'Issues with invalid merge requests: 2.',
            title: ':warning: Issues with invalid merge requests: 2.',
            color: 'warning',
            fields: invalid_issues_fields
          )
        )

        pending_issues_output = batch_merger_result.slack_attachments[1]

        expect(pending_issues_output).to be_empty
      end
    end

    context 'when security issues have pending MRs (not merged)' do
      it 'includes information about the issues' do
        invalid_issues_output = batch_merger_result.slack_attachments[0]

        expect(invalid_issues_output).to be_empty

        mr1 = double(
          :merge_request,
          iid: 1,
          web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1'
        )

        mr2 = double(
          :merge_request,
          iid: 2,
          web_url: 'https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2'
        )

        issue1 = double(
          :issue,
          iid: 1,
          web_url: 'https://gitlab.com/gitlab-org/security/gitlab/issues/1',
          merge_requests: [mr1, mr2]
        )

        pending_issues_fields = [
          {
            title: 'Security implementation issue: #1',
            value: '<https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1|!1>, <https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2|!2>',
            short: false
          }
        ]

        batch_merger_result.pending[issue1.iid] = issue1.merge_requests
        pending_issues_output = batch_merger_result.slack_attachments[1]

        expect(pending_issues_output).to match(
          a_hash_including(
            fallback: "Issues with merge requests that couldn't be merged: 1.",
            title: ":warning: Issues with merge requests that couldn't be merged: 1.",
            color: 'warning',
            fields: pending_issues_fields
          )
        )
      end
    end
  end
end
