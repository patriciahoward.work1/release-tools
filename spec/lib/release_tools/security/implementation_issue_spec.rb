# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::ImplementationIssue do
  let(:project) do
    double(
      :project,
      iid: 1,
      name: 'GitLab'
    )
  end

  let(:web_url) do
    'https://gitlab.com/gitlab-org/security/gitlab/-/issues/1'
  end

  let(:release_bot) do
    {
      id: described_class::GITLAB_RELEASE_BOT_ID,
      name: 'GitLab Release Bot'
    }
  end

  let(:mr1) do
    double(
      :merge_request,
      state: 'opened',
      target_branch: 'master',
      assignees: [release_bot]
    )
  end

  let(:mr2) do
    double(
      :merge_request,
      state: 'opened',
      target_branch: '12-10-stable-ee',
      assignees: [release_bot]
    )
  end

  let(:mr3) do
    double(
      :merge_request,
      state: 'opened',
      target_branch: '12-9-stable-ee',
      assignees: [release_bot]
    )
  end

  let(:mr4) do
    double(
      :merge_request,
      state: 'opened',
      target_branch: '12-8-stable-ee',
      assignees: [release_bot]
    )
  end

  let(:merge_requests) { [mr1, mr2, mr3, mr4] }

  subject { described_class.new(1, 1, merge_requests, web_url) }

  describe '#project_id' do
    it { expect(subject.project_id).to eq(project.iid) }
  end

  describe '#iid' do
    it { expect(subject.iid).to eq(1) }
  end

  describe '#merge_requests' do
    it { expect(subject.merge_requests).to match_array(merge_requests) }
  end

  describe '#web_url' do
    it { expect(subject.web_url).to eq(web_url) }
  end

  describe '#ready_to_be_processed?' do
    context 'with 4 or more merge requests associated and all of them assigned to GitLab bot' do
      it { is_expected.to be_ready_to_be_processed }
    end

    context 'with less than 4 associated merge requests' do
      let(:merge_requests) { [mr1, mr2] }

      it { is_expected.not_to be_ready_to_be_processed }
    end

    context 'when a merge request is not assigned to the GitLab Release Bot' do
      let(:assignee1) do
        {
          id: 1234,
          name: 'Joe'
        }
      end

      let(:mr4) do
        double(
          :merge_request,
          state: 'merged',
          target_branch: '12-10-stable-ee',
          assignees: [assignee1]
        )
      end

      it { is_expected.not_to be_ready_to_be_processed }
    end

    context 'with a merged merge request targeting master' do
      let(:mr1) do
        double(
          :merge_request,
          state: 'merged',
          target_branch: 'master',
          assignees: [release_bot]
        )
      end

      it { is_expected.not_to be_ready_to_be_processed }
    end

    context 'when security_release_auto_deploy_experiment is enabled' do
      let(:project_id) { described_class::GITLAB_ID }

      subject { described_class.new(project_id, 1, merge_requests, web_url) }

      before do
        enable_feature(:security_release_auto_deploy_experiment)
      end

      context 'with a GitLab security issue' do
        it { is_expected.to be_ready_to_be_processed }
      end

      context 'with a security issue associated to another project' do
        let(:project_id) { 123 }

        it { is_expected.not_to be_ready_to_be_processed }
      end

      context 'with a merged merge request targeting master' do
        let(:mr1) do
          double(
            :merge_request,
            state: 'merged',
            target_branch: 'master',
            assignees: [release_bot]
          )
        end

        it { is_expected.to be_ready_to_be_processed }
      end

      context 'with a merged merge request targeting a stable branch' do
        let(:mr1) do
          double(
            :merge_request,
            state: 'merged',
            target_branch: '13-2-stable-ee',
            assignees: [release_bot]
          )
        end

        it { is_expected.not_to be_ready_to_be_processed }
      end
    end
  end

  describe '#merge_request_targeting_master' do
    it 'returns the merge request targeting master' do
      expect(subject.merge_request_targeting_master).to eq(mr1)
    end
  end

  describe '#merge_requests_targeting_stable' do
    let(:mr5) do
      double(
        :merge_request,
        target_branch: '12-10-stable',
        assignees: [release_bot]
      )
    end

    let(:merge_requests) { [mr1, mr2, mr3, mr5] }

    it 'returns merge requests targeting stable branches' do
      merge_requests_targeting_stable_branches = [
        mr2,
        mr3,
        mr5
      ]

      expect(subject.merge_requests_targeting_stable)
        .to match_array(merge_requests_targeting_stable_branches)
    end
  end

  describe '#project_with_auto_deploy?' do
    context 'with a GitLab or Omnibus security issue' do
      it 'returns true for both projects' do
        issue1 = described_class.new(described_class::GITLAB_ID, 1, merge_requests, '')
        issue2 = described_class.new(described_class::OMNIBUS_ID, 1, merge_requests, '')

        expect(issue1).to be_project_with_auto_deploy
        expect(issue2).to be_project_with_auto_deploy
      end
    end

    context 'with an issue belonging to another project' do
      it 'returns false' do
        issue1 = described_class.new(123, 1, merge_requests, '')

        expect(issue1).not_to be_project_with_auto_deploy
      end
    end
  end
end
