# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::Manager do
  let(:deploy_package) { '13.1.202005220540-7c84ccdc806.59f00bb0515' }
  let(:package_version) { ReleaseTools::Version.new(deploy_package) }
  let(:version) { ReleaseTools::Version.new('13.1.0') }

  subject(:manager) { described_class.new(package_version) }

  before do
    allow(ReleaseTools::ReleaseManagers::Schedule)
      .to receive(:new)
      .and_return(instance_spy(ReleaseTools::ReleaseManagers::Schedule, version_for_date: version))
  end

  describe '#monthly_version' do
    it 'get the target monthly version from a the release managers schedule' do
      schedule = double('schedule')
      expect(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)

      expect(schedule)
        .to receive(:active_version)
        .and_return(version)

      expect(manager.monthly_version).to eq("13.1.0")
    end
  end

  describe '#issue' do
    it 'works on the monthly issue' do
      expect(manager.issue.version).to eq(manager.monthly_version)
    end
  end

  describe '#release_manager' do
    subject(:release_manager) { manager.send(:release_manager) }

    it 'reads the username from RELEASE_MANAGER env variable' do
      definitions = double('Definitions')
      allow(ReleaseTools::ReleaseManagers::Definitions).to receive(:new).and_return(definitions)
      expect(definitions).to receive(:find_user)
        .with('ops_username', instance: :ops)
        .and_return(double('User', production: 'username'))

      ClimateControl.modify(RELEASE_MANAGER: 'ops_username') do
        expect(release_manager).to eq('@username')
      end
    end

    it 'tracks unknown release managers' do
      ClimateControl.modify(RELEASE_MANAGER: 'ops-unknown') do
        expect(release_manager).to eq(":warning: Unknown release manager! OPS username ops-unknown")
      end
    end
  end

  describe '#execute' do
    let(:fake_client) { spy("ReleaseTools::GitlabClient") }

    before do
      stub_const("ReleaseTools::GitlabClient", fake_client)
    end

    it 'updates the issue' do
      expect(manager).to receive(:release_manager).and_return('@a-user')

      expect(ReleaseTools::Promotion::StatusNote).to receive(:new)
        .with(
          status: manager.status,
          package_version: package_version,
          ci_pipeline_url: 'pipeline_url',
          release_manager: '@a-user',
          override_status: false,
          override_reason: 'false'
        ).and_return(double('note', body: 'a comment'))

      ClimateControl.modify(CI_PIPELINE_URL: 'pipeline_url') do
        without_dry_run do
          manager.execute
        end
      end

      expect(fake_client)
        .to have_received(:create_issue_note)
        .with(
          ReleaseTools::Project::Release::Tasks,
          issue: manager.issue,
          body: 'a comment'
        )
    end

    it 'raises an exception if the system is not fine' do
      expect(manager.status).to receive(:fine?).and_return(false)
      expect(manager).to receive(:create_issue_comment)

      without_dry_run do
        expect { manager.execute }.to raise_error(ReleaseTools::Promotion::Manager::UnsafeProductionError)
      end
    end

    context 'when IGNORE_PRODUCTION_CHECKS is set' do
      let(:ignore_production_checks) { 'a reason to ignore checks' }

      it 'updates the issue with the reason why it was overridden' do
        expect(manager).to receive(:release_manager).and_return('@a-user')

        expect(ReleaseTools::Promotion::StatusNote).to receive(:new)
          .with(
            status: manager.status,
            package_version: package_version,
            ci_pipeline_url: 'pipeline_url',
            release_manager: '@a-user',
            override_status: true,
            override_reason: ignore_production_checks
          ).and_return(double('note', body: 'a comment'))

        ClimateControl.modify(
          CI_PIPELINE_URL: 'pipeline_url',
          IGNORE_PRODUCTION_CHECKS: ignore_production_checks
        ) do
          without_dry_run do
            manager.execute
          end
        end

        expect(fake_client)
          .to have_received(:create_issue_note)
          .with(
            ReleaseTools::Project::Release::Tasks,
            issue: manager.issue,
            body: 'a comment'
          )
      end

      it "doesn't raise an exception if the system is not fine" do
        expect(manager.status).to receive(:fine?).and_return(false)
        expect(manager).to receive(:create_issue_comment)

        ClimateControl.modify(IGNORE_PRODUCTION_CHECKS: ignore_production_checks) do
          without_dry_run do
            expect { manager.execute }.not_to raise_error
          end
        end
      end
    end
  end
end
