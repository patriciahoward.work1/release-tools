# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::ProductionStatus do
  subject(:status) { described_class.new }

  shared_examples 'a safe system' do
    describe '#fine?' do
      it { is_expected.to be_fine }
    end

    describe '#blockers' do
      it 'has no blockers' do
        blockers = status.blockers

        aggregate_failures do
          blockers.each do |type, issues|
            expect(issues).to be_empty, "expected no #{type}, got #{issues.inspect}"
          end
        end
      end
    end
  end

  shared_examples 'an unsafe system' do
    describe '#fine?' do
      it { is_expected.not_to be_fine }
    end

    describe '#blockers' do
      it 'has blockers' do
        blockers = status.blockers.any? { |_, issues| !issues.empty? }

        expect(blockers).to be_truthy
      end
    end
  end

  context 'when there are no issues on the production tracker' do
    before do
      allow(status).to receive(:issues).and_return([])
    end

    it_behaves_like 'a safe system'
  end

  context 'when there are C3 and C4 change issues on the production tracker' do
    before do
      issues = [
        double('C3', labels: %w(change::in-progress C3)),
        double('C4', labels: %w(change::in-progress C4))
      ]
      allow(status).to receive(:issues).with(labels: 'change::in-progress').and_return(issues)

      allow(status).to receive(:issues).with(labels: 'Incident::Active').and_return([])
    end

    it_behaves_like 'a safe system'
  end

  context 'when there are C1 change issues on the production tracker' do
    before do
      issues = [
        double('C1', labels: %w(change::in-progress C1)),
        double('C4', labels: %w(change::in-progress C4))
      ]
      allow(status).to receive(:issues).with(labels: 'change::in-progress').and_return(issues)

      allow(status).to receive(:issues).with(labels: 'Incident::Active').and_return([])
    end

    it_behaves_like 'an unsafe system'
  end

  context 'when there are C2 change issues on the production tracker' do
    before do
      issues = [
        double('C2', labels: %w(change::in-progress C2), title: 'Change1', url: 'https://change1'),
        double('C4', labels: %w(change::in-progress C4))
      ]
      allow(status).to receive(:issues).with(labels: 'change::in-progress').and_return(issues)

      allow(status).to receive(:issues).with(labels: 'Incident::Active').and_return([])
    end

    it_behaves_like 'an unsafe system'
  end

  context 'when there is an active S1 incident on the production tracker' do
    before do
      allow(status).to receive(:issues).with(labels: 'Incident::Active').and_return([double('incident', labels: %w(Incident::Active S1))])
      allow(status).to receive(:issues).with(labels: 'change::in-progress').and_return([])
    end

    it_behaves_like 'an unsafe system'
  end

  context 'when there is an active S2 incident on the production tracker' do
    before do
      allow(status).to receive(:issues).with(labels: 'Incident::Active').and_return([double('incident', labels: %w(Incident::Active S2))])
      allow(status).to receive(:issues).with(labels: 'change::in-progress').and_return([])
    end

    it_behaves_like 'an unsafe system'
  end

  context 'when there is an active S3 incident on the production tracker' do
    before do
      allow(status).to receive(:issues).with(labels: 'Incident::Active').and_return([double('incident', labels: %w(Incident::Active S3))])
      allow(status).to receive(:issues).with(labels: 'change::in-progress').and_return([])
    end

    it_behaves_like 'an unsafe system'
  end

  context 'when there is an active S4 incident on the production tracker' do
    before do
      allow(status).to receive(:issues).with(labels: 'Incident::Active').and_return([double('incident', labels: %w(Incident::Active S4))])
      allow(status).to receive(:issues).with(labels: 'change::in-progress').and_return([])
    end

    it_behaves_like 'a safe system'
  end

  context 'when production_status feature_flag is disabled' do
    it 'never fetches issues' do
      disable_feature(:production_status)

      client = double('ReleaseTools::GitlabClient')
      stub_const('ReleaseTools::GitlabClient', client)
      expect(client).not_to receive(:issues)

      expect(status).to be_fine
    end
  end
end
