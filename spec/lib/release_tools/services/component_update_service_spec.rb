# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::ComponentUpdateService do
  let(:target_branch) { 'test-auto-deploy-001' }

  subject(:service) { described_class.new(target_branch) }

  describe '#execute' do
    it 'delegates to UpdateComponentService' do
      enable_all_features

      described_class::COMPONENTS.each do |component|
        service = double("UpdateComponentService #{component.project_name}")
        expect(ReleaseTools::Services::UpdateComponentService).to receive(:new)
                                            .with(component, target_branch)
                                            .and_return(service)

        expect(service).to receive(:execute)
      end

      service.execute
    end
  end
end
