# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::MonthlyPreparationService do
  let(:internal_client) { spy('ReleaseTools::GitlabClient') }
  let(:version) { ReleaseTools::Version.new('12.1.0') }
  let(:remote_repo) { double(cleanup: true) }
  let(:version_manager) { double(:next_version) }

  subject(:service) { described_class.new(version) }

  before do
    stub_const('ReleaseTools::GitlabClient', internal_client)

    allow(ReleaseTools::RemoteRepository).to receive(:get).with(ReleaseTools::Project::HelmGitlab.remotes).and_return(remote_repo)
    allow(ReleaseTools::Helm::VersionManager).to receive(:new).with(remote_repo).and_return(version_manager)
    allow(version_manager).to receive(:next_version).with(version.to_s).and_return(ReleaseTools::HelmChartVersion.new("4.3.0"))
  end

  describe '#create_label' do
    it 'does nothing on a dry run' do
      expect(ReleaseTools::PickIntoLabel).not_to receive(:create)

      service.create_label
    end

    it 'is idempotent' do
      expect(internal_client).to receive(:create_group_label)
        .and_raise(gitlab_error(:BadRequest, message: 'Label already exists'))

      without_dry_run do
        expect { service.create_label }.not_to raise_error
      end
    end

    it 'creates the label' do
      label_spy = spy
      stub_const('ReleaseTools::PickIntoLabel', label_spy)

      without_dry_run do
        service.create_label
      end

      expect(label_spy).to have_received(:create).with(version)
    end
  end

  describe '#create_stable_branches' do
    let(:auto_deploy_branch) { '13-0-auto-deploy-20200520' }

    before do
      allow(ReleaseTools::AutoDeployBranch)
        .to receive(:current_name)
        .and_return(auto_deploy_branch)
    end

    context 'when auto-deploy branch is used as source' do
      let(:last_deployment) do
        double(
          'deployment',
          sha: '123abc',
          ref: '13-0-auto-deploy-20200520',
          created_at: '2019-12-17'
        )
      end

      before do
        allow(internal_client).to receive(:last_deployment)
          .with(ReleaseTools::Project::GitlabEe, 1_178_942)
          .and_return(last_deployment)
      end

      it 'does nothing on a dry run' do
        expect(internal_client).not_to receive(:find_or_create_branch)

        service.create_stable_branches(auto_deploy_branch)
      end

      it 'creates stable branches from the last deployment for GitLab, GitLab FOSS and Omnibus' do
        without_dry_run do
          service.create_stable_branches(auto_deploy_branch)
        end

        expect(internal_client).to have_received(:find_or_create_branch)
          .with('12-1-stable-ee', last_deployment.sha, ReleaseTools::Project::GitlabEe)

        expect(internal_client).to have_received(:find_or_create_branch)
          .with('12-1-stable', '12-0-stable', ReleaseTools::Project::GitlabCe)

        expect(internal_client).to have_received(:find_or_create_branch)
          .with('12-1-stable', last_deployment.ref, ReleaseTools::Project::OmnibusGitlab)
      end

      it 'creates stable branches from master for CNGImage and Helm' do
        without_dry_run do
          service.create_stable_branches(auto_deploy_branch)
        end

        expect(internal_client).to have_received(:find_or_create_branch)
          .with('12-1-stable', 'master', ReleaseTools::Project::CNGImage)

        expect(internal_client).to have_received(:find_or_create_branch)
          .with('4-3-stable', 'master', ReleaseTools::Project::HelmGitlab)
      end
    end

    context 'when a SHA is used' do
      let(:sha) { '6dbf7917e82701341e6d9056392d7b4b98673ef9' }

      it 'creates the stable branch from SHA for GitLab, GitLab FOSS' do
        without_dry_run do
          service.create_stable_branches(sha)
        end

        expect(internal_client).to have_received(:find_or_create_branch)
          .with('12-1-stable-ee', sha, ReleaseTools::Project::GitlabEe)

        expect(internal_client).to have_received(:find_or_create_branch)
          .with('12-1-stable', '12-0-stable', ReleaseTools::Project::GitlabCe)
      end

      it 'creates the Omnibus stable branch from the current auto-deploy branch' do
        without_dry_run do
          service.create_stable_branches(sha)
        end

        expect(internal_client).to have_received(:find_or_create_branch)
          .with('12-1-stable', auto_deploy_branch, ReleaseTools::Project::OmnibusGitlab)
      end

      it 'creates stable branches from master for CNGImage and Helm' do
        without_dry_run do
          service.create_stable_branches(sha)
        end

        expect(internal_client).to have_received(:find_or_create_branch)
          .with('12-1-stable', 'master', ReleaseTools::Project::CNGImage)

        expect(internal_client).to have_received(:find_or_create_branch)
          .with('4-3-stable', 'master', ReleaseTools::Project::HelmGitlab)
      end
    end
  end
end
