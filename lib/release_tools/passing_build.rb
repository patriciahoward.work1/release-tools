# frozen_string_literal: true

module ReleaseTools
  class PassingBuild
    include ::SemanticLogger::Loggable

    attr_reader :ref

    def initialize(ref)
      @project = ReleaseTools::Project::GitlabEe
      @ref = ref
    end

    def execute
      commits = ReleaseTools::Commits.new(@project, ref: ref)

      commit =
        if SharedStatus.security_release?
          # Passing builds on dev are few and far between; for a security
          # release we'll just use the latest commit on the branch
          commits.latest
        else
          merge_base = if ReleaseTools::Feature.enabled?(:update_gitaly)
                         # when we update gitaly on master branch, we no longer have to limit
                         # our search to the merge_base
                         nil
                       else
                         commits.merge_base('master')
                       end
          commits.latest_successful_on_build(limit: merge_base)
        end

      if commit.nil?
        raise "Unable to find a passing #{@project} build for `#{ref}` on dev"
      end

      commit
    end
  end
end
