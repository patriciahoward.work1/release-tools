# frozen_string_literal: true

module ReleaseTools
  module Security
    class SyncRemotesService
      include ::SemanticLogger::Loggable
      include ReleaseTools::Services::SyncBranchesHelper

      # Syncs branches across different remotes:
      # * For GitLab and Omnibus GitLab, master and auto-deploy branches
      #   are synced.
      # * For GitLab FOSS and Gitaly, only master branch is synced.
      def initialize
        @branches = [
          'master',
          ReleaseTools::AutoDeployBranch.current.branch_name
        ]

        # When the auto-deploy branch is only on security, don't sync it
        # See https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/943
        @branches.pop if Feature.enabled?(:auto_deploy_on_security)

        @projects = [
          ReleaseTools::Project::GitlabEe,
          ReleaseTools::Project::GitlabCe,
          ReleaseTools::Project::OmnibusGitlab,
          ReleaseTools::Project::Gitaly
        ]
      end

      def execute
        @projects.each do |project|
          branches = branches_to_sync_for(project)

          logger.info('Syncing branches', project: project, branches: branches)

          sync_branches(project, *branches)
        end
      end

      private

      def branches_to_sync_for(project)
        if projects_with_no_auto_deploy_branch.include?(project)
          ['master']
        else
          @branches
        end
      end

      def projects_with_no_auto_deploy_branch
        [
          ReleaseTools::Project::Gitaly,
          ReleaseTools::Project::GitlabCe
        ]
      end
    end
  end
end
