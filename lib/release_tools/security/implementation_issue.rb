# frozen_string_literal: true

module ReleaseTools
  module Security
    class ImplementationIssue
      # Number of merge requests that has to be associated to every Security Issue
      MERGE_REQUESTS_SIZE = 4

      # Internal ID of GitLab Release Bot
      GITLAB_RELEASE_BOT_ID = 2_324_599

      # Format of stable branches on GitLab repos
      STABLE_BRANCH_REGEX = /^(\d+-\d+-stable(-ee)?)$/.freeze

      # Internal ID of GitLab Security project
      GITLAB_ID = 15_642_544

      # Internal ID of Omnibus GitLab Security project
      OMNIBUS_ID = 15_667_093

      # Collection of security projects with auto-deploy branch
      PROJECTS_WITH_AUTO_DEPLOY_BRANCH = [GITLAB_ID, OMNIBUS_ID].freeze

      attr_reader :project_id, :iid, :merge_requests, :web_url

      def initialize(project_id, iid, merge_requests, web_url)
        @project_id = project_id
        @iid = iid
        @merge_requests = merge_requests
        @web_url = web_url
      end

      def ready_to_be_processed?
        allowed_project? &&
          merge_requests.length >= MERGE_REQUESTS_SIZE &&
          valid_merge_requests? &&
          merge_requests_assigned_to_the_bot?
      end

      def merge_request_targeting_master
        merge_requests
          .detect { |merge_request| merge_request.target_branch == 'master' }
      end

      def merge_requests_targeting_stable
        merge_requests
          .select { |merge_request| merge_request.target_branch.match?(STABLE_BRANCH_REGEX) }
      end

      def project_with_auto_deploy?
        PROJECTS_WITH_AUTO_DEPLOY_BRANCH.include?(project_id)
      end

      private

      def allowed_project?
        return true unless running_security_experiment?

        @project_id == GITLAB_ID
      end

      def merge_requests_assigned_to_the_bot?
        merge_requests.all? do |merge_request|
          merge_request
            .assignees
            .map { |assignee| assignee.transform_keys(&:to_sym)[:id] }.include?(GITLAB_RELEASE_BOT_ID)
        end
      end

      def valid_merge_requests?
        if running_security_experiment?
          master_state = merge_request_targeting_master&.state

          master_state != 'closed' && merge_requests_targeting_stable.all? { |mr| mr.state == 'opened' }
        else
          merge_requests.all? { |mr| mr.state == 'opened' }
        end
      end

      def running_security_experiment?
        Feature.enabled?(:security_release_auto_deploy_experiment)
      end
    end
  end
end
