# frozen_string_literal: true

module ReleaseTools
  module Security
    class IssuesResult
      def initialize(issues: [], not_ready: [], ready: [])
        @issues = issues
        @not_ready = not_ready
        @ready = ready
      end

      def slack_attachments
        [
          total_issues,
          issues_not_ready_to_be_processed,
          issues_ready_to_be_processed
        ]
      end

      private

      def total_issues
        {
          fallback: "Security Implementation Issues associated: #{@issues.length}.",
          title: ":information_source: Security Implementation Issues associated: #{@issues.length}.",
          color: 'good'
        }
      end

      def issues_not_ready_to_be_processed
        return {} if @not_ready.empty?

        {
          fallback: "Not ready to be processed: #{@not_ready.length}.",
          title: ":status_warning: Issues not ready to be processed: #{@not_ready.length}.",
          fields: issues_details(@not_ready)
        }
      end

      def issues_ready_to_be_processed
        return {} if @ready.empty?

        {
          fallback: "Ready to be processed: #{@ready.length}.",
          title: ":ballot_box_with_check: Issues ready to be processed: #{@ready.length}.",
          fields: issues_details(@ready)
        }
      end

      def issues_details(issues)
        issues.each.map do |security_issue|
          {
            title: "Security implementation issue: ##{security_issue.iid}",
            value: "<#{security_issue.web_url}>",
            short: false
          }
        end
      end
    end
  end
end
