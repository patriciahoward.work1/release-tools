# frozen_string_literal: true

module ReleaseTools
  module Promotion
    class ProductionStatus
      PRODUCTION = Project::Infrastructure::Production

      def blockers
        {
          incidents: active_incidents,
          changes: in_progress_changes
        }
      end

      def fine?
        blockers.values.all?(&:empty?)
      end

      private

      def active_incidents
        @active_incidents ||= issues(labels: 'Incident::Active').select do |issue|
          issue.labels.include?('S1') || issue.labels.include?('S2') || issue.labels.include?('S3')
        end
      end

      def in_progress_changes
        @in_progress_changes ||= issues(labels: 'change::in-progress').select do |issue|
          issue.labels.include?('C1') || issue.labels.include?('C2')
        end
      end

      def issues(labels:)
        return [] unless ::ReleaseTools::Feature.enabled?(:production_status)

        Retriable.retriable do
          GitlabClient.issues(PRODUCTION, state: 'opened', labels: labels)
        end
      end
    end
  end
end
