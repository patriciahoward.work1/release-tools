# frozen_string_literal: true

module ReleaseTools
  module Promotion
    class Manager
      include ::SemanticLogger::Loggable

      class UnsafeProductionError < StandardError; end

      attr_reader :package_version

      def initialize(package_version)
        @package_version = package_version
      end

      def issue
        @issue ||= ReleaseTools::MonthlyIssue.new(version: monthly_version)
      end

      def monthly_version
        @monthly_version ||= ReleaseManagers::Schedule.new.active_version
      end

      def status
        @status ||= ProductionStatus.new
      end

      def execute
        Retriable.retriable do
          create_issue_comment
        end

        raise UnsafeProductionError unless safe?
      end

      private

      def safe?
        status.fine? || ignore_production_checks?
      end

      # ignore_production_checks return the reason for bypassing production checks
      #
      # Production check bypassing starts with a chatops command.
      # If the release manager provides a reason for bypassing the checks,
      # it will be propagated to the deployer, and then to release-tools using
      # the IGNORE_PRODUCTION_CHECKS trigger variable.
      # On every stage this defaults to 'false', meaning that checks should not
      # be ignored.
      #
      # @return [String] the reason for bypassing the production checks, defaults to 'false'
      def ignore_production_checks
        ENV.fetch('IGNORE_PRODUCTION_CHECKS', 'false')
      end

      # ignore_production_checks? verifies if a reason for bypassing production checks was provided
      #
      # @see Manager#ignore_production_checks
      def ignore_production_checks?
        ignore_production_checks != 'false'
      end

      def note
        StatusNote.new(
          status: status,
          package_version: package_version,
          ci_pipeline_url: ENV['CI_PIPELINE_URL'],
          release_manager: release_manager,
          override_status: ignore_production_checks?,
          override_reason: ignore_production_checks
        )
      end

      def release_manager
        ops_user = ENV['RELEASE_MANAGER']
        return ':warning: Unknown release manager! Missing `RELEASE_MANAGER` variable' if ops_user.nil?

        user = ReleaseManagers::Definitions.new.find_user(ops_user, instance: :ops)

        return ":warning: Unknown release manager! OPS username #{ops_user}" if user.nil?

        "@#{user.production}"
      end

      def create_issue_comment
        if SharedStatus.dry_run?
          logger.warn(
            'Dry run mode, no comment will be posted',
            issue: issue.url,
            package_version: package_version,
            production_healthy: status.fine?,
            ignore_production_checks: ignore_production_checks
          )

          return
        end

        GitlabClient.create_issue_note(
          issue.project,
          issue: issue,
          body: note.body
        )
      end
    end
  end
end
