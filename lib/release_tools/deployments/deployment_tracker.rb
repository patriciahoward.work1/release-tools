# frozen_string_literal: true

module ReleaseTools
  module Deployments
    # Tracking of deployments using the GitLab API
    class DeploymentTracker
      include ::SemanticLogger::Loggable

      # A regex to use for ensuring that we only track Gitaly deployments for
      # SHAs, not tagged versions.
      GITALY_SHA_REGEX = /\A[0-9a-f]{40}\z/.freeze

      # A regex that matches Gitaly tagged releases, such as "12.8.1".
      GITALY_TAGGED_RELEASE_REGEX = /\A\d+\.\d+\.\d+(-rc\d+)?\z/.freeze

      # The deployment statuses that we support.
      DEPLOYMENT_STATUSES = Set.new(%w[success failed]).freeze

      # The ref to use for recording Canonical deployments
      CANONICAL_DEPLOY_REF = 'master'

      # A deployment created using the GitLab API
      Deployment = Struct.new(:project_path, :id, :status) do
        def success?
          status == 'success'
        end
      end

      # The name of the staging environment of GitLab.com.
      STAGING = 'gstg'

      # environment - The name of the environment that was deployed to.
      # status - The status of the deployment, such as "success" or "failed".
      # version - The raw deployment version, as passed from the deployer.
      def initialize(environment, status, version)
        @environment = environment
        @status = status
        @version = version
      end

      def qa_commit_range
        unless @environment == STAGING && @status == 'success'
          return []
        end

        current, previous = GitlabClient
          .deployments(
            Project::GitlabEe.auto_deploy_path,
            @environment
          )
          .first(2)
          .map(&:sha)

        [previous, current]
      end

      def track
        logger.info(
          'Recording GitLab deployment',
          environment: @environment,
          status: @status,
          version: @version
        )

        unless DEPLOYMENT_STATUSES.include?(@status)
          raise(
            ArgumentError,
            "The deployment status #{@status} is not supported"
          )
        end

        version = DeploymentVersionParser.new.parse(@version)
        omnibus_version = OmnibusDeploymentVersionParser.new.parse(@version)

        gitlab_deployments = track_gitlab_deployments(version)
        gitaly_deployments = track_gitaly_deployments(version.sha)
        omnibus_deployments = track_omnibus_deployments(omnibus_version)

        (gitlab_deployments + gitaly_deployments + omnibus_deployments).compact
      end

      private

      def create_canonical_deploy?(project, sha)
        return false unless Feature.enabled?(:auto_deploy_on_security)

        # When performing a deploy of an actual security release, the given
        # commit won't yet exist on Canonical, so there's no point tracking it.
        GitlabClient.commit(project.path, ref: sha).present?
      rescue Gitlab::Error::NotFound
        logger.info(
          "Deployed commit missing in Canonical, skipping `master` deployment",
          project: project.path,
          environment: @environment,
          sha: sha
        )

        false
      end

      def create_deployments(project, ref:, sha:, is_tag:)
        deployments = []

        logger.info(
          "Recording #{project.name.demodulize} deployment",
          environment: @environment,
          status: @status,
          sha: sha,
          ref: ref
        )

        data = GitlabClient.create_deployment(
          project.auto_deploy_path,
          @environment,
          ref,
          sha,
          @status,
          tag: is_tag
        )

        deployments << Deployment.new(project.auto_deploy_path, data.id, data.status)

        return deployments unless create_canonical_deploy?(project, sha)

        # When `auto_deploy_on_security` is enabled, the above deployment was
        # created on the Security mirror.
        #
        # However, merge requests on Canonical still need to be notified about
        # the deployment, but the given ref won't exist there, so we re-create
        # the deployment on Canonical using `master` as a workaround.
        data = GitlabClient.create_deployment(
          project.path,
          @environment,
          CANONICAL_DEPLOY_REF,
          sha,
          @status,
          tag: is_tag
        )

        deployments << Deployment.new(project.path, data.id, data.status)
      end

      def track_gitlab_deployments(version)
        create_deployments(
          Project::GitlabEe,
          ref: version.ref,
          sha: version.sha,
          is_tag: version.tag?
        )
      end

      def track_gitaly_deployments(gitlab_sha)
        version = GitlabClient
          .file_contents(
            Project::GitlabEe.auto_deploy_path,
            Project::Gitaly.version_file,
            gitlab_sha
          )
          .strip

        ref, sha, is_tag = gitaly_deployment_details(version)

        create_deployments(
          Project::Gitaly,
          ref: ref,
          sha: sha,
          is_tag: is_tag
        )
      end

      def gitaly_deployment_details(version)
        if version.match?(GITALY_TAGGED_RELEASE_REGEX)
          tag = GitlabClient
            .tag(Project::Gitaly.auto_deploy_path, tag: "v#{version}")

          [tag.name, tag.commit.id, true]
        elsif version.match?(GITALY_SHA_REGEX)
          [CANONICAL_DEPLOY_REF, version, false]
        else
          raise "The Gitaly version #{version} is not recognised"
        end
      end

      def track_omnibus_deployments(version)
        create_deployments(
          Project::OmnibusGitlab,
          ref: version.ref,
          sha: version.sha,
          is_tag: version.tag?
        )
      end
    end
  end
end
