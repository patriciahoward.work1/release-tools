# frozen_string_literal: true

module ReleaseTools
  module PublicRelease
    # A monhtly release of Gitaly using the API.
    #
    # This class is only concerned with monthly releases. If a release from the
    # `master` branch needs to be created, use `GitalyMasterRelease` instead.
    class GitalyMonthlyRelease
      include Release
      include GitalyRelease

      attr_reader :version, :client, :release_metadata

      def initialize(
        version,
        client: GitlabClient,
        release_metadata: ReleaseMetadata.new
      )
        @version = version.to_ce
        @client = client
        @release_metadata = release_metadata
      end

      def execute
        if version.rc?
          logger.info(
            'Monthly Gitaly releases are only for stable releases',
            version: version
          )

          return
        end

        logger.info('Starting release of Gitaly', version: version)

        create_target_branch
        compile_changelog
        update_versions

        tag = create_tag

        update_gitaly_server_version
        add_release_metadata(tag)
        notify_slack(project, version)
      end

      def compile_changelog
        logger.info('Compiling changelog', project: project_path)

        Changelog::Compiler
          .new(project_path, client: client)
          .compile(version, branch: target_branch)
      end

      def update_gitaly_server_version
        file = project.version_file

        # We bump in both CE and EE, so that we don't have to wait for an EE to
        # CE sync when tagging GitLab EE/CE.
        commit_version_files(
          target_branch,
          file => version,
          message: "Update #{file} to #{version}",
          project: ce_project_path
        )

        commit_version_files(
          ee_stable_branch,
          file => version,
          message: "Update #{file} to #{version}",
          project: ee_project_path
        )
      end

      def source_for_target_branch
        branch = ee_stable_branch
        ee_path = ee_project_path
        content = Retriable.retriable(on: Gitlab::Error::ResponseError) do
          client.file_contents(ee_path, project.version_file, branch).strip
        end

        if content.include?('.')
          Version.new(content).tag
        else
          content
        end
      end

      def ee_stable_branch
        version.stable_branch(ee: true)
      end

      def ee_project_path
        Project::GitlabEe.canonical_or_security_path
      end

      def ce_project_path
        Project::GitlabCe.canonical_or_security_path
      end
    end
  end
end
