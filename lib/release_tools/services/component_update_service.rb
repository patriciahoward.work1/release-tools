# frozen_string_literal: true

module ReleaseTools
  module Services
    # Automatically updates component versions to their latest passing refs
    # @deprecated Use {UpdateComponentService} instead
    class ComponentUpdateService
      include ::SemanticLogger::Loggable

      COMPONENTS = [
        Project::Gitaly,
        Project::GitlabElasticsearchIndexer,
        Project::GitlabPages,
        Project::GitlabShell,
        Project::GitlabWorkhorse
      ].freeze

      attr_reader :target_branch

      def initialize(target_branch)
        @target_branch = target_branch
      end

      def execute
        COMPONENTS.each do |component|
          next unless ::ReleaseTools::Feature.enabled?(:"auto_deploy_#{component.name.demodulize.underscore}")

          UpdateComponentService.new(component, target_branch).execute
        end
      end
    end
  end
end
