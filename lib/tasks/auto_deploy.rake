# frozen_string_literal: true

namespace :auto_deploy do
  task :check_enabled do
    if ReleaseTools::Feature.disabled?(:auto_deploy)
      ReleaseTools.logger.warn("The `auto_deploy` feature flag is currently disabled.")
      exit
    end
  end

  desc "Prepare for auto-deploy by creating branches from the latest green commit on gitlab and omnibus-gitlab"
  task prepare: :check_enabled do
    ReleaseTools::Tasks::AutoDeploy::Prepare.new.execute
  end

  desc 'Pick commits into the auto deploy branches'
  task pick: :check_enabled do
    pick = lambda do |project, branch_name|
      ReleaseTools.logger.info(
        'Picking into auto-deploy branch',
        project: project.auto_deploy_path,
        target: branch_name
      )

      ReleaseTools::CherryPick::AutoDeployService
        .new(project, branch_name)
        .execute
    end

    branch_name = ReleaseTools::AutoDeployBranch.current_name

    pick[ReleaseTools::Project::GitlabEe, branch_name]
    pick[ReleaseTools::Project::OmnibusGitlab, branch_name]

    skip_components_update =
      ReleaseTools::Feature.enabled?(:update_gitaly) ||
      ReleaseTools::Feature.disabled?(:auto_deploy_components)
    ReleaseTools::Services::ComponentUpdateService.new(branch_name).execute unless skip_components_update
  end

  desc "Tag the auto-deploy branches from the latest passing builds"
  task tag: :check_enabled do
    branch = ReleaseTools::AutoDeployBranch.current
    version = "#{branch.version.to_minor}.#{branch.tag_timestamp}"
    metadata = ReleaseTools::ReleaseMetadata.new

    commit = ReleaseTools::PassingBuild
      .new(branch.to_s)
      .execute

    ReleaseTools::AutoDeploy::Builder::Omnibus
      .new(branch, commit.id, metadata)
      .execute

    ReleaseTools::AutoDeploy::Builder::CNGImage
      .new(branch, commit.id, metadata)
      .execute

    ReleaseTools::Deployments::SentryTracker.new.release(commit.id)

    if !dry_run? && ReleaseTools::Feature.enabled?(:release_json_tracking)
      ReleaseTools::ReleaseMetadataUploader.new.upload(version, metadata)
    end
  end

  desc "Validate production pre-checks"
  task :check_production do
    deploy_version = ENV.fetch("DEPLOY_VERSION") do |name|
      raise "This task requires an environment variable, #{name}, containing the package version"
    end

    omnibus_package_version = ReleaseTools::Version.new(deploy_version)
    ReleaseTools::Promotion::Manager.new(omnibus_package_version).execute
  end
end
